<?php
require_once __DIR__.'/vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Entity\Thema;

$dotenv = new Dotenv(__DIR__);
$dotenv->load();
$auth = new Auth(true);
$authenticated = $auth->canEnterProtectedZone();
if($authenticated) {
    $auth->refreshExpiringDate();
}
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Forum</title>
    <link rel="stylesheet" href="src/css/main.css">
    <link rel="stylesheet" href="src/css/colors.css">
    <link rel="stylesheet" href="src/css/listThemes.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="src/js/main.js"></script>
    <script src="src/js/startPage.js"></script>
</head>
<body class="background-gray">
    <?php include_once __DIR__.'/templates/header.php'; ?>

    <div id="list-scrollable-container-startpage">
        <div class="list-scrollable background-gray">
            <?php
            try {
                $thema = new Thema();
                $response = $thema->select(0);
                $themaList = $response->getData()['themas'];
                ?>
                <script language="JavaScript">
                    (function () {
                        offset = parseInt(<?php echo $response->getData()['newOffset']?>);
                    })();
                </script>
            <?php


            foreach ($themaList as $value) {
            ?>
                <div class="shadow-bottom background-white list-element list-element-with-buttons">
                    <div class="list-element-content">
                        <div class="list-element-header">
                            <h2 onclick="location.href = '/sites/postDetails.php?id=<?php echo $value['id'];?>'">
                                <?php echo $value['title'];?>
                            </h2>
                            <span>
                                <?php echo $value['username'];?>
                            </span>
                        </div>
                        <div class="list-element-description">
                            <?php echo $value['description'];?>
                        </div>
                    </div>
                    <div class="list-element-buttons">
                        <?php
                        $visibility = ($value['closed'] === 0) ? 'hidden' : 'visible';
                        ?>
                        <i class="fa fa-lock" style="visibility: <?php echo $visibility;?>"></i>
                    </div>
                </div>
            <?php
            }
            ?>
                <div id="last-list-element">
                    <?php
                    if(count($themaList) === 0) {
                        ?><div class="list-message list-message-show">
                            Dieses Forum ist leer.</div><?php
                    } else if($thema->getLimit() > count($themaList)) {
                        ?><div class="list-message list-message-show">
                            Alle Themen wurden geladen.</div><?php
                    } else {
                        ?><div class="list-message list-message-hide"></div>
                        <?php
                    }
                    ?>
                    <div id="startpage-list-loading-spinner"></div>
                </div><?php
            } catch (Exception $e) {
            ?>
                <div id="last-list-element">
                    <div class="list-message list-message-show">
                        Es ist ein interner Fehler Aufgrtreten.
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

</body>
</html>