<?php

function setActive($id) {
    if(isset($_GET['activeButton']) && $_GET['activeButton'] === $id) {
        return " button-active";
    }else if(!isset($_GET['activeButton']) && $id === '5'){
        return " button-active";
    } else {
        return "";
    }
}

include_once __DIR__.'/alertOverlay.php';
?>
<link rel="stylesheet" href="../src/css/header.css">
<header class="main-header background-darkgray shadow-bottom">
    <div class="header-title-container">
        <h1 class="font-green">&lt; WebEng 2 Forum /&gt;</h1>
    </div>
    <nav class="header-buttons">
        <div onclick="location.href = '/index.php?activeButton=5'"
             class="header-button<?php echo setActive('5'); ?>">
            Startseite
        </div>
        <?php
        if(!$authenticated) {
            ?>
            <div onclick="location.href = '/sites/login.php'"
                 class="header-button<?php echo setActive('0'); ?>">
                Login
            </div>
            <div onclick="location.href = '/sites/registration.php'"
                 class="header-button<?php echo setActive('1'); ?>">
                Registrierung
            </div>
            <?php
        } else {
            ?>
            <div onclick="location.href = '/sites/newPost.php?activeButton=2'"
                 class="header-button<?php echo setActive('2'); ?>">
                Neues Thema
            </div>
            <div onclick="location.href = '/sites/profile.php?activeButton=3'"
                 class="header-button<?php echo setActive('3'); ?>">
                Profil
            </div>
            <div onclick="logout()"
                 class="header-button<?php echo setActive('4'); ?>">
                Logout
            </div>
            <?php
        }
        ?>
    </nav>
</header>
