<div id="opacity-layer" onclick="hideOpacityLayer()"></div>

<div id="delete-profile-alert"
     class="shadow-bottom background-white
     form-container form-size-small alert-dialog">

    <div class="form-input-container">
        <h2>Wollen Sie wirklich ihr Profil löschen?</h2>
        <div class="form-input-checkbox-container">
            <input id="checkbox-delete-all-posts" class="form-input-checkbox" type="checkbox">
            <label for="checkbox-delete-all-posts">Alle Themen mitlöschen</label>
        </div>
        <div class="form-input-checkbox-container">
            <input id="checkbox-delete-all-contributions" class="form-input-checkbox" type="checkbox">
            <label for="checkbox-delete-all-contributions">Alle Beiträge mitlöschen</label>
        </div>
    </div>
    <div class="form-button-container">
        <div onclick="hideOpacityLayer()" class="custom-button button-silver">Abbrechen</div>
        <div onclick="deleteProfile()" class="custom-button button-red">Löschen</div>
    </div>
</div>

<div id="delete-thema-alert" class="shadow-bottom background-white
     form-container form-size-small alert-dialog">

    <div class="form-input-container">
        <h2>Wollen Sie wirklich dieses Thema löschen?</h2>
        <p id="alert-internal-error-msg" class="font-red alert-error-msg">Es ist ein interner Fehler aufgetreten.</p>
        <div id="alert-delete-thema-loading-spinner"></div>
    </div>
    <div class="form-button-container">
        <div onclick="hideOpacityLayer()" class="custom-button button-silver">Abbrechen</div>
        <div id="alert-delete-thema-button" class="custom-button button-red">Löschen</div>
    </div>
</div>

<div id="answer-to-alert" class="shadow-bottom background-white
     form-container form-size-small alert-dialog">

    <div class="form-input-container">
        <h2>Beitrag verfassen</h2>
        <textarea id="alert-beitrag-textarea" class="form-input form-input-textarea" placeholder="Dein Text"></textarea>
        <p id="alert-beitrag-internal-error-msg" class="font-red alert-error-msg">Es ist ein interner Fehler aufgetreten.</p>
        <div id="alert-send-beitrag-loading-spinner"></div>
    </div>
    <div class="form-button-container">
        <div onclick="hideOpacityLayer()" class="custom-button button-silver">Abbrechen</div>
        <div id="alert-send-beitrag-button" class="custom-button button-green">Senden</div>
    </div>
</div>

<div id="delete-beitrag-alert" class="shadow-bottom background-white
     form-container form-size-small alert-dialog">

    <div class="form-input-container">
        <h2>Wollen Sie wirklich diesen Beitrag löschen?</h2>
        <p id="alert-delete-beitrag-internal-error-msg" class="font-red alert-error-msg">Es ist ein interner Fehler aufgetreten.</p>
        <div id="alert-delete-beitrag-loading-spinner"></div>
    </div>
    <div class="form-button-container">
        <div onclick="hideOpacityLayer()" class="custom-button button-silver">Abbrechen</div>
        <div id="alert-delete-beitrag-button" class="custom-button button-red">Löschen</div>
    </div>
</div>