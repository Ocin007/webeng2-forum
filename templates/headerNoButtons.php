<?php
include_once __DIR__.'/alertOverlay.php';
?>
<link rel="stylesheet" href="../src/css/header.css">
<header class="main-header background-darkgray shadow-bottom">
    <div class="header-title-container">
        <h1 class="font-green">&lt; <?php echo $headerNoButtonsTitle; ?> /&gt;</h1>
    </div>
</header>
