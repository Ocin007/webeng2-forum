# webeng2-forum
Matrikelnummer: 2393348

PHP Version: 7.2

Composer version 1.6.3

docker-compose version 1.23.2

Benötigt Internet (wegen jquery und fontawesome)

Bisher nur in Chrome getestet

Vorlage für das Docker-Setup: https://github.com/mzazon/php-apache-mysql-containerized
- - - -

## Installation
- .env.example in .env umbenennen
- Folgende Konfigurationen setzen:
    - DB_ROOT_PASSWORD (Passwort für root)
    - DB_USERNAME      (ein weiterer DB User)
    - DB_PASSWORD      (Passwort des neuen Users)
    - DB_OUTHER_PORT   (der Port der außerhalb des Docker Containers sichtbar ist)
    - DB_INNER_PORT    (der Port innerhalb des Docker Containers)
    - SESSION_DURATION (Dauer in Sekunden wie lange ein inaktiver Benutzer eingeloggt bleibt)
    - SELECT_LIMIT     (Anzahl der Themen, die auf einmal nachgeladen werden)
- Optional: Datenbank mit Testdaten initialisieren (nicht implementiert)
    - die Datei `/src/scripts/init-db/export-plain.sql` nach `/src/scripts` verschieben
    - die Datei `/src/scripts/export-testdata.sql` nach `/src/scripts/init-db` verschieben
- im Ordner `src/js` alle .ts dateien mit ```tsc -p [Ordner src/js]``` übersetzen
- ```docker-compose down -v``` (entfernt den Docker Container inklusive der Datenbank mit allen Daten)
- ```docker-compose up```
- - - -

## Funktionalitäten

- [x] Registrierung
- [x] Login
- [x] Logout
- ##### Wenn nicht eingeloggt
    - [x] Auflistung der Themen auf der Landing-page
    - [x] Auflistung aller Beiträge eines angeklickten Themas
- ##### Wenn eingeloggt
    - [x] Thema
        - [x] Neues Thema erstellen
        - [x] Thema schließen (über Profil)
        - [x] Thema löschen (über Profil)
    - [x] Beitrag
        - [x] Neuen Beitrag zu einem Thema erstellen
        - [x] Auf anderen Beitrag antworten
        - [x] Beitrag löschen
    - [x] Profil bearbeiten
        - [x] Username ändern
        - [x] Passwort ändern
        - [x] Profil löschen (Optionale Auswahl: Themen oder Beiträge mitlöschen)
        - [x] Einzelne Themen schließen / wieder öffnen / löschen
    - [ ] Optional: https://developers.giphy.com/docs/