<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;

$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();
$auth = new Auth(false);
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Forum</title>
    <link rel="stylesheet" href="../src/css/main.css">
    <link rel="stylesheet" href="../src/css/colors.css">
    <link rel="stylesheet" href="../src/css/form.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../src/js/main.js"></script>
    <script src="../src/js/login.js"></script>
</head>
<body class="background-gray">

    <?php
    $headerNoButtonsTitle = 'Login';
    include_once __DIR__.'/../templates/headerNoButtons.php';
    ?>

    <div id="login-container" class="shadow-bottom background-white form-container form-size-small">
        <?php
        if($auth->validateSession()) {
            $auth->refreshExpiringDate();

            ?>
            <h1 class="font-black">Sie sind bereits eingeloggt.</h1>
            <div id="login-loading-spinner"></div>
            <div class="form-button-container">
                <div onclick="location.href = '/index.php?activeButton=5'" class="custom-button button-silver">Startseite</div>
                <div onclick="logout()" class="custom-button button-red">Abmelden</div>
            </div>
            <?php

        } else {

            ?>
            <div class="form-input-container">
                <input onkeypress="loginOnEnter(event)"
                       onchange="rmClassInputInvalidOnChangeLogin('username')"
                       id="username" type="text" class="form-input" placeholder="Benutzername">
                <input onkeypress="loginOnEnter(event)"
                       onchange="rmClassInputInvalidOnChangeLogin('password')"
                       id="password" type="password" class="form-input" placeholder="Passwort">
                <p id="internal-error-msg" class="font-red">Es ist ein interner Fehler aufgetreten.</p>
                <div id="login-loading-spinner"></div>
            </div>
            <div class="form-button-container">
                <div onclick="location.href = '/index.php?activeButton=5'" class="custom-button button-silver">Startseite</div>
                <div onclick="location.href = '/sites/registration.php';" class="custom-button button-silver">Registrieren</div>
                <div onclick="login()" class="custom-button button-green">Anmelden</div>
            </div>
            <?php

        }
        ?>
    </div>

</body>
</html>