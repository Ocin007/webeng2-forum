<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;

$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();
$auth = new Auth(false);
$sessValid = $auth->validateSession();
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <?php
    if($sessValid) {
        ?>
        <meta http-equiv="refresh" content="0; URL=http://<?php echo $_SERVER['HTTP_HOST'] ?>/index.php?activeButton=5">
        <?php
    }
    ?>
    <title>Forum</title>
    <link rel="stylesheet" href="../src/css/main.css">
    <link rel="stylesheet" href="../src/css/colors.css">
    <link rel="stylesheet" href="../src/css/form.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../src/js/main.js"></script>
    <script src="../src/js/registration.js"></script>
</head>
<?php
if(!$sessValid) {
    ?>
    <body class="background-gray">

    <?php
    $headerNoButtonsTitle = 'Registrierung';
    include_once __DIR__.'/../templates/headerNoButtons.php';
    ?>

    <div id="login-container" class="shadow-bottom background-white form-container form-size-small">
        <div class="form-input-container">
            <input onkeyup="registrationOnEnter(event)"
                   id="new-username" type="text" class="form-input" placeholder="Benutzername">
            <input onkeyup="registrationOnEnter(event)"
                   id="new-password" type="password" class="form-input" placeholder="Passwort">
            <input onkeyup="registrationOnEnter(event)"
                   id="repeat-password" type="password" class="form-input" placeholder="Passwort wiederholen">
            <p id="internal-error-msg" class="font-red">Es ist ein interner Fehler aufgetreten.</p>
            <div id="registration-loading-spinner"></div>
        </div>
        <div class="form-button-container">
            <div onclick="location.href = '/index.php?activeButton=5'" class="custom-button button-silver">Startseite</div>
            <div onclick="location.href = '/sites/login.php';" class="custom-button button-silver">Login</div>
            <div onclick="registration()" id="registration-button" class="custom-button button-green">Registrieren</div>
        </div>
    </div>

    </body>
    <?php
} else {
    $auth->refreshExpiringDate();
}
?>
</html>