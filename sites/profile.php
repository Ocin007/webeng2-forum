<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Entity\Thema;

$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();
$auth = new Auth(true);
$authenticated = $auth->canEnterProtectedZone();
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <?php
    if(!$authenticated) {
        ?>
        <meta http-equiv="refresh" content="0; URL=http://<?php echo $_SERVER['HTTP_HOST'] ?>/sites/login.php">
        <?php
    }
    ?>
    <title>Forum</title>
    <link rel="stylesheet" href="../src/css/main.css">
    <link rel="stylesheet" href="../src/css/colors.css">
    <link rel="stylesheet" href="../src/css/form.css">
    <link rel="stylesheet" href="../src/css/listThemes.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../src/js/main.js"></script>
    <script src="../src/js/profile.js"></script>
</head>
<?php
if($authenticated) {
    $auth->refreshExpiringDate();
    ?>
    <body class="background-gray display-form-size-large">
    <?php include_once __DIR__ . '/../templates/header.php'; ?>

    <div class="shadow-bottom background-white form-container form-size-large">
        <div class="form-tabs-container">
            <div onclick="showUserData()" id="form-tab-userdata" class="form-tab">Benutzerdaten</div>
            <div onclick="showPosts()" id="form-tab-posts" class="form-tab">Erstellte Themen</div>
        </div>
        <div id="form-tab-content-userdata" class="form-tab-content">

            <div class="form-input-container">
                <h3>Benutzername ändern</h3>
                <input onkeyup="checkUpdateInput(event)" id="update-username" type="text" class="form-input" placeholder="<?php echo $auth->getUserName(); ?>">
                <h3>Passwort ändern</h3>
                <input onkeyup="checkUpdateInput(event)" id="update-password" type="password" class="form-input" placeholder="Neues Passwort">
                <input onkeyup="checkUpdateInput(event)" id="update-repeat-password" type="password" class="form-input" placeholder="Neues Passwort wiederholen">
                <p id="internal-error-msg" class="font-red">Es ist ein interner Fehler aufgetreten.</p>
                <div id="update-loading-spinner"></div>
            </div>
            <div class="form-button-container">
                <div onclick="showOpacityLayer('delete-profile-alert')" class="custom-button button-red">Profil löschen</div>
                <div onclick="resetChanges()" class="custom-button button-silver">Änderungen zurücksetzen</div>
                <div onclick="updateProfile()" class="custom-button button-green">Speichern</div>
            </div>

        </div>
        <div id="form-tab-content-posts" class="form-tab-content">
            <div class="list-scrollable-container">
                <div class="list-scrollable background-gray">
                    <?php
                    try {
                        $thema = new Thema();
                        $response = $thema->selectWhereUserId(0, $auth->getUserId());
                        $themaList = $response->getData()['themas'];
                        ?>
                        <script language="JavaScript">
                            (function () {
                                offset = parseInt(<?php echo $response->getData()['newOffset']?>);
                            })();
                        </script>
                        <?php


                        foreach ($themaList as $value) {
                            ?>
                            <div class="shadow-bottom background-white list-element list-element-with-buttons">
                                <div class="list-element-content">
                                    <div class="list-element-header">
                                        <h2 onclick="location.href = '/sites/postDetails.php?id=<?php echo $value['id'];?>'">
                                            <?php echo $value['title'];?>
                                        </h2>
                                        <span>
                                            <?php echo $value['username'];?>
                                        </span>
                                    </div>
                                    <div class="list-element-description">
                                        <?php echo $value['description'];?>
                                    </div>
                                </div>
                                <div class="list-element-buttons">
                                    <?php
                                    if($value['closed'] === 0) {
                                        ?><i onclick="toggleLock(event, <?php echo $value['id'];?>)" class="fa fa-unlock"></i><?php
                                    } else {
                                        ?><i onclick="toggleLock(event, <?php echo $value['id'];?>)" class="fa fa-lock"></i><?php
                                    }
                                    ?>
                                    <i onclick="deleteThema(<?php echo $value['id'];?>)" class="fa fa-trash-o"></i>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <div id="last-list-element">
                        <?php
                        if(count($themaList) === 0) {
                            ?><div class="list-message list-message-show">
                                Sie haben noch kein Thema erstellt.</div><?php
                        } else if($thema->getLimit() > count($themaList)) {
                            ?><div class="list-message list-message-show">
                                Alle Themen wurden geladen.</div><?php
                        } else {
                            ?><div class="list-message list-message-hide"></div>
                                    <?php
                        }
                        ?>
                        <div id="profile-list-loading-spinner"></div>
                        </div><?php
                    } catch (Exception $e) {
                        ?>
                        <div id="last-list-element">
                            <div class="list-message list-message-show">
                                Es ist ein interner Fehler Aufgrtreten.
                            </div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    </body>
    <?php
}
?>
</html>