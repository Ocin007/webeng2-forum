<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Entity\Beitrag;
use ForumApi\Database\Entity\Thema;

$dotenv = new Dotenv(__DIR__ . '/..');
$dotenv->load();
$auth = new Auth(true);
$authenticated = $auth->canEnterProtectedZone();
if ($authenticated) {
    $auth->refreshExpiringDate();
}

function printPost($post, $thema, Auth $auth)
{
    echo '<div class="single-post">';
    if($post['username'] === null) {
        echo '<h3 class="post-user-deleted">Benutzer gelöscht</h3>';
    } else {
        echo '<h3>'.$post['username'].'<span>'.$post['created'].'</span></h3>';
    }
    if($post['text'] === null) {
        echo '<div class="post-content post-content-deleted">Beitrag wurde vom Benutzer gelöscht.</div>';
    } else {
        echo '<div class="post-content">'.$post['text'].'</div>';
    }
    if($thema['closed'] === 0 && $auth->canEnterProtectedZone()) {
        echo '<div class="answer-delete-button-container">';
        echo '  <div onclick="answerTo('.$thema['id'].', '.$post['id'].')" class="answer-button">Antworten</div>';
        if($post['fk_userid'] === $auth->getUserId()) {
            echo '<i onclick="deleteContribution('.$thema['id'].', '.$post['id'].')" class="fa fa-trash-o"></i>';
        }
        echo '</div>';
    }
    echo '</div>';
}

function generateHtml($posts, $thema, Auth $auth, int $answersTo = null)
{
    $answerPosts = [];
    foreach ($posts as $post) {
        if($post['fk_beitragid'] === $answersTo) {
            $answerPosts[] = $post;
        }
    }
    if(count($answerPosts) === 0) {
        return;
    }
    if($answersTo === null) {
        echo '<div>';
    } else {
        echo '<div class="post-layer">';
    }
    foreach ($answerPosts as $answerPost) {
        printPost($answerPost, $thema, $auth);
        generateHtml($posts, $thema, $auth, $answerPost['id']);
    }
    echo '</div>';
}

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Forum</title>
    <link rel="stylesheet" href="../src/css/main.css">
    <link rel="stylesheet" href="../src/css/colors.css">
    <link rel="stylesheet" href="../src/css/postDetails.css">
    <link rel="stylesheet" href="../src/css/form.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../src/js/main.js"></script>
    <script src="../src/js/postDetails.js"></script>
</head>
<body class="background-gray body-thema-detail">
    <?php
    $_GET['activeButton'] = 'highlight no button';
    include_once __DIR__ . '/../templates/header.php';

    $foundThema = false;
    try {
        if(isset($_GET['id'])) {
            $themaEntity = new Thema();
            $responseThema = $themaEntity->selectWhereThemaId(intval($_GET['id']));
            $thema = $responseThema->getData()['thema'];
            if($thema['id'] === intval($_GET['id']) && $thema['id'] !== null) {
                $foundThema = true;
            }
        }
        if (!$foundThema) {
            ?>
            <div class="thema-not-found-container">
                <h1 class="font-light-gray">404</h1>
                <h1 class="font-light-gray">Thema wurde nicht gefunden</h1>
            </div>
            <?php
        } else {
            $postEntity = new Beitrag();
            $responsePost = $postEntity->selectWhereThemaId($thema['id']);
            $posts = $responsePost->getData()['posts'];

            ?>
            <div class="shadow-bottom background-white thema-container">
                <h1 class="thema-title">
                    <?php echo $thema['title']; ?>
                </h1>
                <div class="thema-description">
                    <?php echo $thema['description']; ?>
                </div>
                <div class="thema-button-container">
                    <?php
                    if($thema['closed'] === 0 && $authenticated) {
                        ?><div onclick="answerTo(<?php echo $thema['id']; ?>)" class="custom-button button-green">Beitrag erstellen</div><?php
                    } else if(!$authenticated) {
                        ?><h2>Melde dich an um Beiträge zu verfassen.</h2><?php
                    } else {
                        ?><h2>Dieses Thema ist geschlossen.</h2><?php
                    }
                    ?>
                </div>
                <div class="post-container">
                    <?php
                    generateHtml($posts, $thema, $auth);
                    ?>
                </div>
            </div>
            <?php
        }
    } catch (Exception $e) {
        ?>
        <div class="thema-not-found-container">
            <h1 class="font-light-gray">500</h1>
            <h1 class="font-light-gray">Es ist ein interner Fehler Aufgrtreten</h1>
        </div>
        <?php
    }
?>
</body>
</html>
