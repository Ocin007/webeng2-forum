<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;

$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();
$auth = new Auth(true);
$authenticated = $auth->canEnterProtectedZone();
?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <?php
    if(!$authenticated) {
        ?>
        <meta http-equiv="refresh" content="0; URL=http://<?php echo $_SERVER['HTTP_HOST'] ?>/sites/login.php">
        <?php
    }
    ?>
    <title>Forum</title>
    <link rel="stylesheet" href="../src/css/main.css">
    <link rel="stylesheet" href="../src/css/colors.css">
    <link rel="stylesheet" href="../src/css/form.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="../src/js/main.js"></script>
    <script src="../src/js/newPost.js"></script>
</head>
<?php
if($authenticated) {
    $auth->refreshExpiringDate();
    ?>
    <body class="background-gray">
    <?php include_once __DIR__ . '/../templates/header.php'; ?>

    <div class="shadow-bottom background-white form-container form-size-medium">
        <div class="form-input-container">
            <input onkeyup="newPostRemoveAllClasses()" id="new-post-title" type="text" class="form-input" placeholder="Titel">
            <textarea onkeyup="newPostRemoveAllClasses()" id="new-post-description" class="form-input form-input-textarea" placeholder="Beschreibung"></textarea>
            <p id="internal-error-msg" class="font-red">Es ist ein interner Fehler aufgetreten.</p>
            <div id="new-post-loading-spinner"></div>
        </div>
        <div class="form-button-container">
            <div onclick="createNewPost()" class="custom-button button-green">Erstellen</div>
        </div>
    </div>

    </body>
    <?php
}
?>
</html>