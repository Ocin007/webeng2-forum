CREATE TABLE IF NOT EXISTS `Forum`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL UNIQUE,
  `password` VARCHAR(255) NOT NULL,
  `created` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `modified` DATETIME ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `Forum`.`Thema` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `created` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `fk_userid` INT NULL,
  `closed` TINYINT(1) NOT NULL DEFAULT 0,
  CHECK (`closed` = 0 OR `closed` = 1),
  PRIMARY KEY (`id`),
  INDEX `fk_userid_idx` (`fk_userid` ASC) VISIBLE,
  CONSTRAINT `fk_userid`
  FOREIGN KEY (`fk_userid`)
  REFERENCES `Forum`.`User` (`id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

CREATE TABLE `Forum`.`Beitrag` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fk_themaid` INT NOT NULL,
  `text` TEXT NULL,
  `created` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `fk_userid` INT NULL,
  `fk_beitragid` INT NULL,
  `isDeleted` TINYINT(1) NOT NULL DEFAULT 0,
  CHECK (`isDeleted` = 0 OR `isDeleted` = 1),
  PRIMARY KEY (`id`),
  INDEX `fk_themaid_idx` (`fk_themaid` ASC) VISIBLE,
  INDEX `fk_userid_idx` (`fk_userid` ASC) VISIBLE,
  CONSTRAINT `fk_themaid`
  FOREIGN KEY (`fk_themaid`)
  REFERENCES `Forum`.`Thema` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_userid_beitrag`
  FOREIGN KEY (`fk_userid`)
  REFERENCES `Forum`.`User` (`id`)
    ON DELETE SET NULL
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;

ALTER TABLE `Forum`.`Beitrag`
  ADD INDEX `fk_beitragid_idx` (`fk_beitragid` ASC) VISIBLE;

ALTER TABLE `Forum`.`Beitrag`
  ADD CONSTRAINT `fk_beitragid`
FOREIGN KEY (`fk_beitragid`)
REFERENCES `Forum`.`Beitrag` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;