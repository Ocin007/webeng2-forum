<?php

namespace ForumApi;


class Response
{
    private $code;
    private $msg;
    private $data;

    public function __construct(int $code = 200, string $msg = 'ok')
    {
        $this->code = $code;
        $this->msg = $msg;
        $this->data = [];
    }

    public function append(array $data)
    {
        $this->data = array_merge($this->data, $data);
    }

    public function getData(): array
    {
        return $this->data;
    }

    function send(array $data = [])
    {
        $this->data = array_merge($this->data, $data);
        $this->data['code'] = $this->code;
        $this->data['message'] = $this->msg;
        echo json_encode($this->data);
    }


}