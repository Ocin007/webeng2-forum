<?php

namespace ForumApi;


use ForumApi\Database\Entity\User;

class Auth
{
    const SESS_KEY_USER_ID = 'userId';
    const SESS_KEY_USER_NAME = 'username';
    const SESS_KEY_EXPIRES_AT = 'expiresAt';

    private $protected;
    private $sessionId;

    public function __construct(bool $protected)
    {
        $this->protected = $protected;
        session_start();
        $this->sessionId = session_id();
    }

    function canEnterProtectedZone()
    {
        return !$this->protected || ($this->protected && $this->validateSession());
    }

    /**
     * @param string $username
     * @param string $password
     * @return Response
     * @throws \Exception
     */
    function login(string $username, string $password): Response
    {
        if($this->validateSession()) {
            $this->refreshExpiringDate();
            return new Response(200, 'already logged in');
        }
        $userEntity = new User();
        $user = $userEntity->getUserWithUsername($username);
        if(isset($user['password']) && password_verify($password, $user['password'])) {
            $this->sessionId = session_id();
            $this->refreshExpiringDate();
            $_SESSION[self::SESS_KEY_USER_ID] = $user['id'];
            $_SESSION[self::SESS_KEY_USER_NAME] = $user['username'];
            return new Response();
        }
        session_destroy();
        $res = new Response(401, 'Unauthorized: wrong login credentials');
        return $res;
    }

    function validateSession(): bool
    {
        return $this->sessionId !== '' &&
            isset($_SESSION[self::SESS_KEY_USER_ID]) &&
            isset($_SESSION[self::SESS_KEY_USER_NAME]) &&
            isset($_SESSION[self::SESS_KEY_EXPIRES_AT]) &&
            getdate()[0] < $_SESSION[self::SESS_KEY_EXPIRES_AT];
    }

    function refreshExpiringDate()
    {
        $_SESSION[self::SESS_KEY_EXPIRES_AT] = getdate()[0] + getenv('SESSION_DURATION');
    }

    function getUserId(): int
    {
        return $_SESSION[self::SESS_KEY_USER_ID];
    }

    function getUserName(): string
    {
        return $_SESSION[self::SESS_KEY_USER_NAME];
    }

    function logout()
    {
        session_destroy();
        $this->sessionId = '';
    }
}