<?php

namespace ForumApi\Database\Entity;


use ForumApi\Response;

interface Entity
{
    public function insert(): Response;
    public function deleteWhereUserId(int $id): Response;
}