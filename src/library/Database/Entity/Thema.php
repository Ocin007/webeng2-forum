<?php

namespace ForumApi\Database\Entity;


use ForumApi\Database\Connection;
use ForumApi\Response;

class Thema implements Entity
{
    private $title;
    private $description;
    private $fk_userid;
    private $connection;
    private $conn;
    private $limit;

    /**
     * Thema constructor.
     * @param string|null $title
     * @param string|null $description
     * @param int $fk_userid
     * @throws \Exception
     */
    public function __construct(
        string $title = null,
        string $description = null,
        int $fk_userid = null)
    {
        $this->connection = Connection::getInstance();
        $this->conn = $this->connection->get();
        $this->title = $title;
        $this->description = $description;
        $this->fk_userid = $fk_userid;
        $this->limit = getenv('SELECT_LIMIT');
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function insert(): Response
    {
        if(!$this->newThemaIsValid()) {
            return new Response(400, 'Bad Request');
        }
        $sql = "INSERT INTO Thema (title, description, fk_userid) VALUES (?, ?, ?)";
        $stmt = $this->conn->prepare($sql);
        if($stmt === false) {
            throw new \Exception('could not prepare query');
        }
        $stmt->bind_param('ssi', $this->title, $this->description, $this->fk_userid);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function deleteWhereUserId(int $id): Response
    {
        $this->removeFKinBeitragWhereUserId($id);
        $sql = "DELETE FROM Thema WHERE fk_userid = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function deleteWhereThemaId(int $id): Response
    {
        $this->removeFKinBeitragWhereThemaId($id);
        $sql = "DELETE FROM Thema WHERE id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    private function newThemaIsValid(): bool
    {
        return $this->fk_userid !== null &&
            $this->title !== null &&
            strlen($this->title) > 0 &&
            strlen($this->title) <= 255 &&
            !preg_match('/^[ ]*$/', $this->title);
    }

    /**
     * @param string $sql
     * @return \mysqli_stmt
     * @throws \Exception
     */
    private function createStmt(string $sql): \mysqli_stmt
    {
        $stmt = $this->conn->prepare($sql);
        if($stmt === false) {
            throw new \Exception('could not prepare query');
        }
        return $stmt;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    private function removeFKinBeitragWhereUserId(int $id)
    {
        $sql = "UPDATE Beitrag b
                  INNER JOIN Thema t ON b.fk_themaid = t.id
                  INNER JOIN User u ON t.fk_userid = u.id
                SET b.fk_beitragid = NULL
                  where u.id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    private function removeFKinBeitragWhereThemaId(int $id)
    {
        $sql = "UPDATE Beitrag b SET b.fk_beitragid = NULL
                  where b.fk_themaid = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param int $offset
     * @return Response
     * @throws \Exception
     */
    public function select(int $offset): Response
    {
        $sql = "SELECT t.id, t.title, t.description, t.closed, u.username FROM Thema t
                  JOIN User u on t.fk_userid = u.id
                  ORDER BY t.created DESC 
                  LIMIT ?,?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('ii', $offset, $this->limit);
        $res = $this->executeSelect($stmt);
        $res->append(['newOffset' => $this->limit+$offset]);
        return $res;
    }

    /**
     * @param int $offset
     * @param int $userId
     * @return Response
     * @throws \Exception
     */
    public function selectWhereUserId(int $offset, int $userId): Response
    {
        $sql = "SELECT t.id, t.title, t.description, t.closed, u.username FROM Thema t
                  JOIN User u on t.fk_userid = u.id
                  WHERE u.id = ?
                  ORDER BY t.created DESC 
                  LIMIT ?,?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('iii', $userId, $offset, $this->limit);
        $res = $this->executeSelect($stmt);
        $res->append(['newOffset' => $this->limit+$offset]);
        return $res;
    }

    private function executeSelect(\mysqli_stmt $stmt): Response
    {
        $res = new Response();
        $resList = [];
        $stmt->execute();
        $stmt->bind_result($id, $title, $description, $closed, $username);
        while ($stmt->fetch()) {
            $resList[] = [
                'id' => $id,
                'title' => $title,
                'description' => $description,
                'closed' => $closed,
                'username' => $username
            ];
        }
        $stmt->close();
        $res->append(['themas' => $resList]);
        return $res;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function toggleClosed(int $id): Response
    {
        $closed = $this->getClosedOfThemaId($id);
        $closed = ($closed === 0) ? 1 : 0;
        $sql = "UPDATE Thema SET closed = ? WHERE id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('ii', $closed, $id);
        $stmt->execute();
        $stmt->close();
        $res = new Response();
        $res->append(['closed' => $closed]);
        return $res;
    }

    /**
     * @param int $id
     * @return int
     * @throws \Exception
     */
    public function getClosedOfThemaId(int $id): int
    {
        $sql = "SELECT t.closed FROM Thema t WHERE t.id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($closed);
        $stmt->fetch();
        $stmt->close();
        return $closed;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function selectWhereThemaId(int $id): Response
    {
        $sql = "SELECT t.id, t.title, t.description, t.closed, u.username FROM Thema t
                  JOIN User u on t.fk_userid = u.id
                  WHERE t.id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($id, $title, $description, $closed, $username);
        $stmt->fetch();
        $stmt->close();
        $res = new Response();
        $res->append(['thema' => [
            'id' => $id,
            'title' => $title,
            'description' => $description,
            'closed' => $closed,
            'username' => $username
        ]]);
        return $res;
    }
}