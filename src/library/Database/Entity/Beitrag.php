<?php

namespace ForumApi\Database\Entity;


use ForumApi\Database\Connection;
use ForumApi\Response;

class Beitrag implements Entity
{
    private $themaId;
    private $themaIsClosed;
    private $text;
    private $createdBy;
    private $answersTo;
    private $connection;
    private $conn;

    /**
     * Beitrag constructor.
     * @param int|null $themaId
     * @param string|null $text
     * @param int|null $createdBy
     * @param int|null $answersTo
     * @throws \Exception
     */
    public function __construct(
        int $themaId = null,
        string $text = null,
        int $createdBy = null,
        int $answersTo = null
    )
    {
        $this->connection = Connection::getInstance();
        $this->conn = $this->connection->get();
        $this->themaId = $themaId;
        $this->text = $text;
        $this->createdBy = $createdBy;
        $this->answersTo = $answersTo;
        $this->themaIsClosed = false;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function deleteWhereUserId(int $id): Response
    {
        $res = new Response();
        $res->append(
            $this->completeDeleteWhereUserId($id)->getData()
        );
        $res->append(
            $this->partialDeleteWhereUserId($id)->getData()
        );
        return $res;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function deleteWhereBeitragId(int $id): Response
    {
        if($this->selectWhereAnswerTo($id)->getData()['count'] === 0) {
            $current = $this->selectWhereBeitragId($id)->getData()['post'];
            do {
                if($current['fk_beitragid'] !== null) {
                    $child = $this->selectWhereBeitragId($current['fk_beitragid'])->getData()['post'];
                    $this->completeDeleteWhereBeitragId($current['beitragId']);
                } else {
                    $this->completeDeleteWhereBeitragId($current['beitragId']);
                    break;
                }
                if($child['isDeleted'] === 1) {
                    $current = $child;
                }
            } while ($child['isDeleted'] === 1);
            return new Response();
        }
        $sql = "UPDATE Beitrag SET text = NULL, isDeleted = 1 WHERE id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    private function completeDeleteWhereBeitragId(int $id): Response
    {
        $sql = "DELETE FROM Beitrag WHERE id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    private function selectWhereAnswerTo(int $id): Response
    {
        $res = new Response();
        $sql = "SELECT COUNT(*) FROM Beitrag b WHERE b.fk_beitragid = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($count);
        $stmt->fetch();
        $stmt->close();
        $res->append(['count' => $count]);
        return $res;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    private function completeDeleteWhereUserId(int $id): Response
    {
        $sql = "DELETE b2 FROM Beitrag b1
                  RIGHT JOIN Beitrag b2 on b1.fk_beitragid = b2.id
                  WHERE b1.fk_beitragid IS NULL AND (b2.fk_userid = ? OR b2.isDeleted = 1)";
        $sql2 = "SELECT COUNT(b2.id) count FROM Beitrag b1
                  RIGHT JOIN Beitrag b2 on b1.fk_beitragid = b2.id
                  WHERE b1.fk_beitragid IS NULL AND (b2.fk_userid = ? OR b2.isDeleted = 1)";
        $delete = $this->createStmt($sql);
        $select = $this->createStmt($sql2);
        $delete->bind_param('i', $id);
        $select->bind_param('i', $id);
        $select->bind_result($count);
        $select->execute();
        $select->store_result();
        $select->fetch();
        $select->free_result();
        $countSteps = 0;
        while ($count > 0) {
            $countSteps++;
            $delete->execute();
            $select->bind_result($count);
            $select->execute();
            $select->store_result();
            $select->fetch();
            $select->free_result();
        }
        $delete->close();
        $select->close();
        $res = new Response();
        $res->append(['steps' => $countSteps]);
        return $res;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    private function partialDeleteWhereUserId(int $id): Response
    {
        $sql = "UPDATE Beitrag SET text = NULL, isDeleted = 1 WHERE fk_userid = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function insert(): Response
    {
        if(!$this->newBeitragIsValid()) {
            if($this->themaIsClosed) {
                return new Response(403, 'Forbidden');
            }
            return new Response(400, 'Bad Request');
        }
        $sql = "INSERT INTO Beitrag (fk_themaid, text, fk_userid, fk_beitragid) VALUES (?, ?, ?, ?)";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('isii', $this->themaId, $this->text, $this->createdBy, $this->answersTo);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param string $sql
     * @return \mysqli_stmt
     * @throws \Exception
     */
    private function createStmt(string $sql): \mysqli_stmt
    {
        $stmt = $this->conn->prepare($sql);
        if($stmt === false) {
            throw new \Exception('could not prepare query');
        }
        return $stmt;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function selectWhereThemaId(int $id): Response
    {
        $res = new Response();
        $resList = [];
        $sql = "SELECT b.id, b.text, b.created, b.fk_beitragid, b.isDeleted, b.fk_userid, u.username FROM Beitrag b
                  LEFT JOIN User u on b.fk_userid = u.id
                WHERE b.fk_themaid = ?
                ORDER BY b.created DESC";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($beitragId, $text, $created, $fk_beitragid, $isDeleted, $fk_userid, $username);
        while ($stmt->fetch()) {
            $resList[] = [
                'id' => $beitragId,
                'text' => $text,
                'created' => $created,
                'fk_beitragid' => $fk_beitragid,
                'isDeleted' => $isDeleted,
                'fk_userid' => $fk_userid,
                'username' => $username
            ];
        }
        $stmt->close();
        $res->append(['posts' => $resList]);
        return $res;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function newBeitragIsValid(): bool
    {
        if($this->themaId === null) {
            return false;
        }
        $thema = new Thema();
        $closed = $thema->getClosedOfThemaId($this->themaId);
        if($closed === 1) {
            $this->themaIsClosed = true;
            return false;
        }
        if($this->text === null || $this->text === '' || $this->createdBy === null) {
            return false;
        }
        if($this->answersTo !== null) {
            $contr = $this->selectWhereBeitragId($this->answersTo)->getData()['post'];
            if($contr['fk_themaid'] !== $this->themaId) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function selectWhereBeitragId(int $id): Response
    {
        $res = new Response();
        $sql = "SELECT * FROM Beitrag WHERE id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($beitragId, $fk_themaid, $text, $created, $fk_userid, $fk_beitragid, $isDeleted);
        $stmt->fetch();
        $stmt->close();
        $res->append(['post' => [
            'beitragId' => $beitragId,
            'fk_themaid' => $fk_themaid,
            'text' => $text,
            'created' => $created,
            'fk_userid' => $fk_userid,
            'fk_beitragid' => $fk_beitragid,
            'isDeleted' => $isDeleted
        ]]);
        return $res;
    }
}