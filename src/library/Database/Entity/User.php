<?php

namespace ForumApi\Database\Entity;


use ForumApi\Database\Connection;
use ForumApi\Response;

class User implements Entity
{
    private $username;
    private $password;
    private $password2;
    private $passwordHash;
    private $connection;
    private $conn;

    private $pswdsNotMatch = false;
    private $usrAlrExist = false;
    private $usrEmpty = false;
    private $pswdEmpty = false;
    private $pswd2Empty = false;

    /**
     * User constructor.
     * @param string|null $username
     * @param string|null $password
     * @param string|null $password2
     * @throws \Exception
     */
    public function __construct(
        string $username = null,
        string $password = null,
        string $password2 = null)
    {
        $this->connection = Connection::getInstance();
        $this->conn = $this->connection->get();
        $this->username = $username;
        $this->password = $password;
        $this->password2 = $password2;
    }

    /**
     * @return Response
     * @throws \Exception
     */
    public function insert(): Response
    {
        if(!$this->newUserIsValid()) {
            $res = new Response(400, 'Bad Request');
            $res->append([
                "pswdsNotMatch" => $this->pswdsNotMatch,
                "usrAlrExist" => $this->usrAlrExist,
                "usrEmpty" => $this->usrEmpty,
                "pswdEmpty" => $this->pswdEmpty,
                "pswd2Empty" => $this->pswd2Empty
            ]);
            return $res;
        }
        $sql = "INSERT INTO User (username, password) VALUES (?, ?)";
        $stmt = $this->createStmt($sql);
        $this->passwordHash = password_hash($this->password, PASSWORD_DEFAULT);
        $stmt->bind_param('ss', $this->username, $this->passwordHash);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function update(int $id): Response
    {
        if(!$this->updateValuesValid()) {
            $res = new Response(400, 'Bad Request');
            $res->append([
                "pswdsNotMatch" => $this->pswdsNotMatch,
                "usrAlrExist" => $this->usrAlrExist,
                "usrEmpty" => $this->usrEmpty,
                "pswdEmpty" => $this->pswdEmpty,
                "pswd2Empty" => $this->pswd2Empty
            ]);
            return $res;
        }
        $userEmpty = $this->username === null || $this->username === '';
        $passEmpty = $this->password === null || $this->password === '';
        if(!$userEmpty && !$passEmpty) {
            $sql = "UPDATE User SET User.username = ?, User.password = ? WHERE id = ?";
            $stmt = $this->createStmt($sql);
            $this->passwordHash = password_hash($this->password, PASSWORD_DEFAULT);
            $stmt->bind_param('ssi', $this->username, $this->passwordHash, $id);
        } else if(!$userEmpty) {
            $sql = "UPDATE User SET User.username = ? WHERE id = ?";
            $stmt = $this->createStmt($sql);
            $stmt->bind_param('si', $this->username, $id);
        } else {
            $sql = "UPDATE User SET User.password = ? WHERE id = ?";
            $stmt = $this->createStmt($sql);
            $this->passwordHash = password_hash($this->password, PASSWORD_DEFAULT);
            $stmt->bind_param('si', $this->passwordHash, $id);
        }
        $stmt->execute();
        $stmt->close();
        return new Response();
    }

    /**
     * @param string $sql
     * @return \mysqli_stmt
     * @throws \Exception
     */
    private function createStmt(string $sql): \mysqli_stmt
    {
        $stmt = $this->conn->prepare($sql);
        if($stmt === false) {
            throw new \Exception('could not prepare query');
        }
        return $stmt;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function newUserIsValid(): bool
    {
        if($this->username === null || $this->username === '') {
            $this->usrEmpty = true;
        }
        if($this->password === null || $this->password === '') {
            $this->pswdEmpty = true;
        }
        if($this->password2 === null || $this->password2 === '') {
            $this->pswd2Empty = true;
        }
        if(!$this->pswdEmpty && !$this->pswd2Empty &&
        $this->password !== $this->password2) {
            $this->pswdsNotMatch = true;
        }
        if(!$this->usrEmpty) {
            $user = $this->getUserWithUsername($this->username);
            if($user['id'] !== null) {
                $this->usrAlrExist = true;
            }
        }
        return !($this->pswdsNotMatch ||
            $this->usrAlrExist ||
            $this->usrEmpty ||
            $this->pswdEmpty ||
            $this->pswd2Empty);
    }

    /**
     * @return bool
     * @throws \Exception
     */
    private function updateValuesValid(): bool
    {
        $userEmpty = $this->username === null || $this->username === '';
        $passEmpty = $this->password === null || $this->password === '';
        $pass2Empty = $this->password2 === null || $this->password2 === '';
        if($userEmpty && $passEmpty && $pass2Empty) {
            $this->usrEmpty = true;
            $this->pswdEmpty = true;
            $this->pswd2Empty = true;
            return false;
        }
        if(!$userEmpty) {
            $existingUser = $this->getUserWithUsername($this->username);
            if ($existingUser['id'] !== null) {
                $this->usrAlrExist = true;
            }
        }
        if($passEmpty && !$pass2Empty) {
            $this->pswdEmpty = true;
        }
        if(!$passEmpty && $pass2Empty) {
            $this->pswd2Empty = true;
        }
        if(!$passEmpty && !$pass2Empty && $this->password !== $this->password2) {
            $this->pswdsNotMatch = true;
        }
        return !($this->pswdsNotMatch ||
            $this->usrAlrExist ||
            $this->pswdEmpty ||
            $this->pswd2Empty);
    }

    /**
     * @param string $username
     * @return mixed
     * @throws \Exception
     */
    public function getUserWithUsername(string $username): array
    {
        $sql = "SELECT * FROM User where User.username = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('s', $username);
        $stmt->execute();
        $stmt->bind_result($id, $uname, $pswd, $created, $modified);
        $stmt->fetch();
        $stmt->close();
        return [
            'id' => $id,
            'username' => $uname,
            'password' => $pswd,
            'created' => $created,
            'modified' => $modified
        ];
    }

    /**
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function getUserWithId(int $id): array
    {
        $sql = "SELECT * FROM User where User.id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->bind_result($userid, $uname, $pswd, $created, $modified);
        $stmt->fetch();
        $stmt->close();
        return [
            'id' => $userid,
            'username' => $uname,
            'password' => $pswd,
            'created' => $created,
            'modified' => $modified
        ];
    }

    /**
     * @param int $id
     * @return Response
     * @throws \Exception
     */
    public function deleteWhereUserId(int $id): Response
    {
        $sql = "DELETE FROM User WHERE id = ?";
        $stmt = $this->createStmt($sql);
        $stmt->bind_param('i', $id);
        $stmt->execute();
        $stmt->close();
        return new Response();
    }
}