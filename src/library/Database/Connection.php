<?php

namespace ForumApi\Database;


use mysqli;

class Connection
{
    private static $instance = null;
    private $conn;

    /**
     * Connection constructor.
     * @throws \Exception
     */
    private function __construct()
    {
        $host = getenv('DB_HOST');
        $user = getenv('DB_USERNAME');
        $pass = getenv('DB_PASSWORD');
        $schema = getenv('DB_NAME');
        $port = getenv('DB_INNER_PORT');
        $this->conn = new mysqli($host, $user, $pass, $schema, $port);
        if ($this->conn->connect_error) {
            throw new \Exception('could not connect to database');
        }
    }

    /**
     * @return Connection
     * @throws \Exception
     */
    public static function getInstance(): Connection
    {
        if( self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function get(): mysqli
    {
        return $this->conn;
    }

    public function disconnect()
    {
        $this->conn->close();
    }
}