function loginOnEnter(ev: KeyboardEvent) {
    if(ev.key !== 'Enter') {
        rmClassInputInvalidOnChangeLogin(ev.srcElement.id);
        return;
    }
    login();
}

function rmClassInputInvalidOnChangeLogin(id: string) {
    if(id === 'username') {
        $('#username').removeClass('input-invalid');
    } else {
        $('#password').removeClass('input-invalid');
    }
}

function login() {
    let username = $('#username').val();
    let password = $('#password').val();
    let url = '../api/login.php';
    toggleLoadingSpinner('login-loading-spinner');
    $.post(url, {username: username, password: password}, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('login-loading-spinner');
            try {
                if(res.code === 200) location.href = '/index.php';
                else if(res.code >= 400 && res.code < 500) {
                    $('#username').addClass('input-invalid');
                    $('#password').addClass('input-invalid');
                    $('#internal-error-msg').css('visibility', 'hidden');
                } else {
                    console.error(res);
                    $('#internal-error-msg').css('visibility', 'visible');
                }
            } catch (e) {
                console.error(e);
                console.error(res);
                $('#internal-error-msg').css('visibility', 'visible');
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('login-loading-spinner');
            let errMsg = $('#internal-error-msg');
            errMsg.css('visibility', 'visible');
            errMsg.text(xhr.status + ' ' + error);
        });
}