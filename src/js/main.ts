let offset = 0;

type OneThema = {
    id: number
    title: string
    description: string|null
    closed: number
    username: string|null
};

function logout() {
    let url = '../api/logout.php';
    toggleLoadingSpinner('login-loading-spinner');
    $.post(url, '', null, 'json')
        .done((res) => {
            toggleLoadingSpinner('login-loading-spinner');
            if(res.code === 401) {
                console.warn('Code: 401, Message: '+res.message+', already logged out');
            }
            location.href = '../sites/login.php';
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('login-loading-spinner');
            console.error(xhr);
            console.error(status);
            console.error(xhr.status + ' ' + error);
        });
}

function toggleLoadingSpinner(id: string) {
    let parent = $('#'+id);
    let spinner = $(
        '<div class="spinner">' +
        '<div class="bounce1"></div>' +
        '<div class="bounce2"></div>' +
        '<div class="bounce3"></div>' +
        '</div>');
    if(parent.children('.spinner').length) {
        parent.children('.spinner').remove();
    } else {
        parent.html(spinner);
    }
}

function showInvalidIfPasswordDontMatch(idPass: string, idPass2: string) {
    let pswd2Element = $('#' + idPass2);
    let password = $('#' + idPass).val();
    let password2 = pswd2Element.val();
    if(password !== password2) {
        pswd2Element.addClass('input-invalid');
    } else {
        pswd2Element.removeClass('input-invalid');
    }
}

function displayWrongInputInfo(res: RegInputErr, userId: string, passId: string, pass2Id: string) {
    if(res.usrAlrExist || res.usrEmpty) {
        $('#' + userId).addClass('input-invalid');
    }
    if(res.pswdEmpty) {
        $('#' + passId).addClass('input-invalid');
    }
    if(res.pswdsNotMatch || res.pswd2Empty) {
        $('#' + pass2Id).addClass('input-invalid');
    }
}

function showOpacityLayer(id: string) {
    $('#opacity-layer').css('visibility', 'visible');
    $('#' + id).css('visibility', 'visible');
}

function hideOpacityLayer() {
    $('#opacity-layer').css('visibility', 'hidden');
    $('.alert-dialog').css('visibility', 'hidden');
    $('.alert-error-msg').css('visibility', 'hidden');
}

function loadListShowMsg(msg?: string) {
    let msgContainer = $('#last-list-element').children('.list-message');
    if(msg === undefined) {
        msgContainer.removeClass('list-message-show');
        msgContainer.addClass('list-message-hide');
    } else {
        msgContainer.addClass('list-message-show');
        msgContainer.removeClass('list-message-hide');
        msgContainer.text(msg);
    }
}

function insertNewElements(generateFunc: (thema: OneThema) => any, themas: Array<OneThema>) {
    let lastElem = $('#last-list-element');
    for (let i = 0; i < themas.length; i++) {
        lastElem.before(generateFunc(themas[i]));
    }
}