function createNewPost() {
    let titleElem = $('#new-post-title');
    let descriptionElem = $('#new-post-description');
    let title = titleElem.val();
    let description = descriptionElem.val();
    let url = '../api/theme/create.php';
    toggleLoadingSpinner('new-post-loading-spinner');
    $.post(url, {title: title, description: description}, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('new-post-loading-spinner');
            newPostRemoveAllClasses();
            if(res.code === 200) {
                newPostSuccess();
            } else if(res.code === 400) {
                newPostErr();
            } else if(res.code === 401) {
                location.href = '/sites/login.php';
            } else {
                console.error(res);
                $('#internal-error-msg').css('visibility', 'visible');
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('new-post-loading-spinner');
            let errMsg = $('#internal-error-msg');
            errMsg.css('visibility', 'visible');
            errMsg.text(xhr.status + ' ' + error);
        });
}

function newPostRemoveAllClasses() {
    let titleElem = $('#new-post-title');
    let descriptionElem = $('#new-post-description');
    titleElem.removeClass('input-valid');
    titleElem.removeClass('input-invalid');
    descriptionElem.removeClass('input-valid');
    descriptionElem.removeClass('input-invalid');
    $('#internal-error-msg').css('visibility', 'hidden');
}

function newPostErr() {
    let titleElem = $('#new-post-title');
    titleElem.addClass('input-invalid');
}

function newPostSuccess() {
    let titleElem = $('#new-post-title');
    let descriptionElem = $('#new-post-description');
    titleElem.addClass('input-valid');
    titleElem.val('');
    descriptionElem.addClass('input-valid');
    descriptionElem.val('');
}