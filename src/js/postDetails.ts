function answerTo(themaId: number, id?: number) {
    let sendButton = $('#alert-send-beitrag-button');
    sendButton.off();
    sendButton.on('click', () => {
        let text = $('#alert-beitrag-textarea').val();
        let body = {};
        if(id === undefined) {
            body = {themaId: themaId, text: text};
        } else {
            body = {themaId: themaId, id: id, text: text};
        }
        let url = '../api/contribution/create.php';
        toggleLoadingSpinner('alert-send-beitrag-loading-spinner');
        $.post(url, body, null, 'json')
            .done((res) => {
                toggleLoadingSpinner('alert-send-beitrag-loading-spinner');
                if(res.code === 200) {
                    hideOpacityLayer();
                    location.href = '/sites/postDetails.php?id='+themaId;
                } else if(res.code === 400) {
                    $('#alert-beitrag-internal-error-msg').css('visibility', 'visible')
                        .text('Die Anfrage war fehlerhaft.');
                } else if(res.code === 401) {
                    $('#alert-beitrag-internal-error-msg').css('visibility', 'visible')
                        .text('Ihre Sitzung ist abgelaufen.');
                    location.href = '/sites/postDetails.php?id=' + themaId;
                } else if(res.code === 403) {
                    $('#alert-beitrag-internal-error-msg').css('visibility', 'visible')
                        .text('Dieses Thema ist geschlossen. Es können keine Beiträge verfasst werden.');
                } else {
                    console.error(res);
                    $('#alert-beitrag-internal-error-msg').css('visibility', 'visible')
                        .text('Es ist ein interner Fehler aufgetreten.');
                }
            })
            .fail((xhr, status, error) => {
                toggleLoadingSpinner('alert-send-beitrag-loading-spinner');
                let errMsg = $('#alert-beitrag-internal-error-msg');
                errMsg.css('visibility', 'visible');
                errMsg.text(xhr.status + ' ' + error);
            });
    });
    showOpacityLayer('answer-to-alert');
}

function deleteContribution(themaId: number, id: number) {
    let delButton = $('#alert-delete-beitrag-button');
    delButton.off();
    delButton.on('click', () => {
        let url = '../api/contribution/delete.php';
        toggleLoadingSpinner('alert-delete-beitrag-loading-spinner');
        $.post(url, {id: id}, null, 'json')
            .done((res) => {
                toggleLoadingSpinner('alert-delete-beitrag-loading-spinner');
                if(res.code === 200) {
                    hideOpacityLayer();
                    location.href = '/sites/postDetails.php?id='+themaId;
                } else if(res.code === 400) {
                    $('#alert-delete-beitrag-internal-error-msg').css('visibility', 'visible')
                        .text('Die Anfrage war fehlerhaft.');
                } else if(res.code === 401) {
                    $('#alert-delete-beitrag-internal-error-msg').css('visibility', 'visible')
                        .text('Ihre Sitzung ist abgelaufen.');
                    location.href = '/sites/postDetails.php?id=' + themaId;
                } else {
                    console.error(res);
                    $('#alert-delete-beitrag-internal-error-msg').css('visibility', 'visible')
                        .text('Es ist ein interner Fehler aufgetreten.');
                }
            })
            .fail((xhr, status, error) => {
                toggleLoadingSpinner('alert-delete-beitrag-loading-spinner');
                let errMsg = $('#alert-delete-beitrag-internal-error-msg');
                errMsg.css('visibility', 'visible');
                errMsg.text(xhr.status + ' ' + error);
            });
    });
    showOpacityLayer('delete-beitrag-alert');
}