function registrationOnEnter(ev: KeyboardEvent) {
    if(ev.key !== 'Enter') {
        rmClassInputInvalidOnChangeRegistr(ev.srcElement.id);
        return;
    }
    registration();
}

function rmClassInputInvalidOnChangeRegistr(id: string) {
    if(id === 'new-username') {
        $('#new-username').removeClass('input-invalid');
    } else if(id === 'new-password') {
        showInvalidIfPasswordDontMatch('new-password', 'repeat-password');
        $('#new-password').removeClass('input-invalid');
    } else {
        showInvalidIfPasswordDontMatch('new-password', 'repeat-password');
    }
}

function registration() {
    let username = $('#new-username').val();
    let password = $('#new-password').val();
    let password2 = $('#repeat-password').val();
    let url = '../api/registration.php';
    toggleLoadingSpinner('registration-loading-spinner');
    $.post(url, {username: username, password: password, password2: password2}, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('registration-loading-spinner');
            try {
                if(res.code === 200){
                    loginAfterRegistration(username, password);
                }
                else if(res.code >= 400 && res.code < 500) {
                    displayWrongInputInfo(res, 'new-username', 'new-password', 'repeat-password');
                    $('#internal-error-msg').css('visibility', 'hidden');
                } else {
                    console.error(res);
                    $('#internal-error-msg').css('visibility', 'visible');
                }
            } catch (e) {
                console.error(e);
                console.error(res);
                $('#internal-error-msg').css('visibility', 'visible');
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('registration-loading-spinner');
            let errMsg = $('#internal-error-msg');
            errMsg.css('visibility', 'visible');
            errMsg.text(xhr.status + ' ' + error);
        });
}

function loginAfterRegistration(username: string, password: string) {
    let urlLogin = '../api/login.php';
    toggleLoadingSpinner('registration-loading-spinner');
    $.post(urlLogin, {username: username, password: password}, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('registration-loading-spinner');
            if(res.code === 200) {
                location.href = '/index.php';
            } else {
                let errMsg = $('#internal-error-msg');
                errMsg.css('visibility', 'visible');
                errMsg.text('Registrierung erfolgreich, Login fehlgeschlagen!');
                console.error('Code: '+res.code+', Message: '+res.message);
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('registration-loading-spinner');
            let errMsg = $('#internal-error-msg');
            errMsg.css('visibility', 'visible');
            errMsg.text(xhr.status + ' ' + error);
        });
}

type RegInputErr = {
    code: number,
    message: string,
    pswdsNotMatch: boolean,
    usrAlrExist: boolean,
    usrEmpty: boolean,
    pswdEmpty: boolean,
    pswd2Empty: boolean
}