document.addEventListener('DOMContentLoaded', () => {
    showUserData();
    let scrolledToBottom = false;
    let loadingLocked = false;
    $('.list-scrollable').scroll((ev) => {
        if($(ev.target).scrollTop() + $(ev.target).height() > ev.target.scrollHeight - 60
            && !scrolledToBottom && !loadingLocked) {
            scrolledToBottom = true;
            loadingLocked = true;
            loadThemas(offset, (newOffset) => {
                loadingLocked = false;
                offset = newOffset;
            });
        }
        if(!($(ev.target).scrollTop() + $(ev.target).height() > ev.target.scrollHeight - 60) &&
            scrolledToBottom) {
            scrolledToBottom = false;
        }
    });
});

function showUserData() {
    $('#form-tab-content-posts').css('display', 'none');
    $('#form-tab-content-userdata').css('display', 'flex');
    $('#form-tab-userdata').addClass('form-tab-active');
    $('#form-tab-posts').removeClass('form-tab-active');
}

function showPosts() {
    $('#form-tab-content-userdata').css('display', 'none');
    $('#form-tab-content-posts').css('display', 'flex');
    $('#form-tab-userdata').removeClass('form-tab-active');
    $('#form-tab-posts').addClass('form-tab-active');
}

function resetChanges() {
    $('#update-username').val('');
    $('#update-password').val('');
    $('#update-repeat-password').val('');
}

function checkUpdateInput(ev: KeyboardEvent) {
    let user = $('#update-username');
    let pass = $('#update-password');
    let pass2 = $('#update-repeat-password');
    user.removeClass('input-valid');
    pass.removeClass('input-valid');
    pass2.removeClass('input-valid');
    switch (ev.srcElement.id) {
        case 'update-username':
            user.removeClass('input-invalid');
            break;
        case 'update-password':
            pass.removeClass('input-invalid');
            showInvalidIfPasswordDontMatch('update-password', 'update-repeat-password');
            break;
        case 'update-repeat-password':
            showInvalidIfPasswordDontMatch('update-password', 'update-repeat-password');
    }
}

function updateProfile() {
    let username = $('#update-username');
    let password = $('#update-password');
    let password2 = $('#update-repeat-password');
    let body = {
        username: (username.val() !== '') ? username.val() : undefined,
        password: (password.val() !== '') ? password.val() : undefined,
        password2: (password2.val() !== '') ? password2.val() : undefined
    };
    let url = '../api/profile/update.php';
    toggleLoadingSpinner('update-loading-spinner');
    $.post(url, body, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('update-loading-spinner');
            if(res.code === 200) {
                updateSuccess(username.val());
            } else if(res.code === 400) {
                displayWrongInputInfo(res, 'update-username', 'update-password', 'update-repeat-password');
            } else if(res.code === 401) {
                location.href = '/sites/login.php';
            } else {
                console.error(res);
                $('#internal-error-msg').css('visibility', 'visible');
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('update-loading-spinner');
            let errMsg = $('#internal-error-msg');
            errMsg.css('visibility', 'visible');
            errMsg.text(xhr.status + ' ' + error);
        });
}

function updateSuccess(newUsername: string) {
    let username = $('#update-username');
    let password = $('#update-password');
    let password2 = $('#update-repeat-password');
    if(newUsername !== '') {
        username.attr('placeholder', newUsername);
    }
    username.val('');
    username.addClass('input-valid');
    username.removeClass('input-invalid');
    password.val('');
    password.addClass('input-valid');
    password.removeClass('input-invalid');
    password2.val('');
    password2.addClass('input-valid');
    password2.removeClass('input-invalid');
}

function deleteProfile() {
    hideOpacityLayer();
    let delAllPosts = $('#checkbox-delete-all-posts').is(':checked');
    let delAllContr = $('#checkbox-delete-all-contributions').is(':checked');
    let url = '../api/profile/delete.php';
    toggleLoadingSpinner('update-loading-spinner');
    $.post(url, {posts: delAllPosts, contributions: delAllContr}, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('update-loading-spinner');
            if(res.code === 200) {
                location.href = '/sites/registration.php';
            } else if(res.code === 400) {
                $('#internal-error-msg').css('visibility', 'visible')
                    .text('Die Anfrage war fehlerhaft.');
            } else if(res.code === 401) {
                $('#internal-error-msg').css('visibility', 'visible')
                    .text('Ihre Sitzung ist abgelaufen.');
            } else {
                console.error(res);
                $('#internal-error-msg').css('visibility', 'visible')
                    .text('Es ist ein interner Fehler aufgetreten.');
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('update-loading-spinner');
            let errMsg = $('#internal-error-msg');
            errMsg.css('visibility', 'visible');
            errMsg.text(xhr.status + ' ' + error);
        });
}

function loadThemas(offset: number, callback: (newOffset: number) => void) {
    let url = '../api/theme/selectWhereUserId.php';
    toggleLoadingSpinner('profile-list-loading-spinner');
    loadListShowMsg();
    $.get(url, {offset: offset}, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('profile-list-loading-spinner');
            if(res.code === 200) {
                insertNewElements(generateNewListElem, res.themas);
                if(res.themas.length === 0) {
                    loadListShowMsg('Alle Themen wurden geladen.');
                }
                callback(res.newOffset);
            } else if(res.code === 400) {
                loadListShowMsg('Die Anfrage war fehlerhaft.');
            } else if(res.code === 401) {
                loadListShowMsg('Ihre Sitzung ist abgelaufen.');
            } else {
                console.error(res);
                loadListShowMsg('Es ist ein interner Fehler aufgetreten.');
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('profile-list-loading-spinner');
            loadListShowMsg(xhr.status + ' ' + error);
        });
}

function generateNewListElem(thema: OneThema) {
    let username = (thema.username === null) ? "Account gelöscht" : thema.username;
    let description = (thema.description === null) ? "" : thema.description;
    let lockClass = (thema.closed === 0) ? "fa fa-unlock" : "fa fa-lock";
    return $("<div class=\"shadow-bottom background-white list-element list-element-with-buttons\">\n" +
        "    <div class=\"list-element-content\">\n" +
        "        <div class=\"list-element-header\">\n" +
        "            <h2 onclick=\"location.href = '/sites/postDetails.php?id="+thema.id+"'\">"+thema.title+"</h2>\n" +
        "            <span>"+username+"</span>\n" +
        "        </div>\n" +
        "        <div class=\"list-element-description\">"+description+"</div>\n" +
        "    </div>\n" +
        "    <div class=\"list-element-buttons\">\n" +
        "        <i onclick='toggleLock(event, "+thema.id+")' class=\""+lockClass+"\"></i>" +
        "        <i onclick='deleteThema("+thema.id+")' class=\"fa fa-trash-o\"></i>\n" +
        "    </div>\n" +
        "</div>");
}

function toggleLock(ev: MouseEvent, id: number) {
    $(ev.target).css('color', 'var(--background-silver)');
    let url = '../api/theme/toggleClosed.php';
    $.post(url, {id: id}, null, 'json')
        .done((res) => {
            if(res.code === 200) {
                $(ev.target).css('color', 'initial');
                if(res.closed === 0) {
                    $(ev.target).addClass('fa-unlock').removeClass('fa-lock');
                } else {
                    $(ev.target).addClass('fa-lock').removeClass('fa-unlock');
                }
            } else if(res.code === 400) {
                $(ev.target).css('color', 'var(--button-red)');
                alert('400 Bad Request: Die Abfrage war fehlerhaft.');
            } else if(res.code === 401) {
                location.href = '/sites/login.php';
            } else {
                console.error(res);
                $('#alert-internal-error-msg').css('visibility', 'visible')
                    .text('Es ist ein interner Fehler aufgetreten.');
            }
        })
        .fail((xhr, status, error) => {
            $(ev.target).css('color', 'var(--button-red)');
            alert(xhr.status + ' ' + error);
        });
}

function deleteThema(id: number) {
    let delButton = $('#alert-delete-thema-button');
    delButton.off();
    delButton.on('click', () => {
        let url = '../api/theme/delete.php';
        toggleLoadingSpinner('alert-delete-thema-loading-spinner');
        $.post(url, {id: id}, null, 'json')
            .done((res) => {
                toggleLoadingSpinner('alert-delete-thema-loading-spinner');
                if(res.code === 200) {
                    hideOpacityLayer();
                    location.href = '/sites/profile.php?activeButton=3';
                } else if(res.code === 400) {
                    $('#alert-internal-error-msg').css('visibility', 'visible')
                        .text('Die Anfrage war fehlerhaft.');
                } else if(res.code === 401) {
                    $('#alert-internal-error-msg').css('visibility', 'visible')
                        .text('Ihre Sitzung ist abgelaufen.');
                } else {
                    console.error(res);
                    $('#alert-internal-error-msg').css('visibility', 'visible')
                        .text('Es ist ein interner Fehler aufgetreten.');
                }
            })
            .fail((xhr, status, error) => {
                toggleLoadingSpinner('alert-delete-thema-loading-spinner');
                let errMsg = $('#alert-internal-error-msg');
                errMsg.css('visibility', 'visible');
                errMsg.text(xhr.status + ' ' + error);
            });
    });
    showOpacityLayer('delete-thema-alert');
}