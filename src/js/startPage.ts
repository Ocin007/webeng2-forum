document.addEventListener('DOMContentLoaded', () => {
    $('#list-scrollable-container-startpage').css('height', innerHeight - 190);
    let scrolledToBottom = false;
    let loadingLocked = false;
    $('.list-scrollable').scroll((ev) => {
        if($(ev.target).scrollTop() + $(ev.target).height() > ev.target.scrollHeight - 60
            && !scrolledToBottom && !loadingLocked) {
            scrolledToBottom = true;
            loadingLocked = true;
            loadAllThemas(offset, (newOffset) => {
                loadingLocked = false;
                offset = newOffset;
            });
        }
        if(!($(ev.target).scrollTop() + $(ev.target).height() > ev.target.scrollHeight - 60) &&
            scrolledToBottom) {
            scrolledToBottom = false;
        }
    });
});
$(window).resize(() => {
    $('#list-scrollable-container-startpage').css('height', innerHeight - 190);
});

function loadAllThemas(offset: number, callback: (newOffset: number) => void) {
    // let url = '../api/theme/selectAll.php';
    let url = 'api/theme/selectAll.php';
    toggleLoadingSpinner('startpage-list-loading-spinner');
    loadListShowMsg();
    $.get(url, {offset: offset}, null, 'json')
        .done((res) => {
            toggleLoadingSpinner('startpage-list-loading-spinner');
            if(res.code === 200) {
                insertNewElements(generateNewListElemStartpage, res.themas);
                if(res.themas.length === 0) {
                    loadListShowMsg('Alle Themen wurden geladen.');
                }
                callback(res.newOffset);
            } else if(res.code === 400) {
                loadListShowMsg('Die Anfrage war fehlerhaft.');
            } else if(res.code === 401) {
                loadListShowMsg('Ihre Sitzung ist abgelaufen.');
            } else {
                console.error(res);
                loadListShowMsg('Es ist ein interner Fehler aufgetreten.');
            }
        })
        .fail((xhr, status, error) => {
            toggleLoadingSpinner('startpage-list-loading-spinner');
            loadListShowMsg(xhr.status + ' ' + error);
        });
}

function generateNewListElemStartpage(thema: OneThema) {
    let username = (thema.username === null) ? "Account gelöscht" : thema.username;
    let description = (thema.description === null) ? "" : thema.description;
    let visibility = (thema.closed === 0) ? "hidden" : "visible";
    return $("<div class=\"shadow-bottom background-white list-element list-element-with-buttons\">\n" +
        "    <div class=\"list-element-content\">\n" +
        "        <div class=\"list-element-header\">\n" +
        "            <h2 onclick=\"location.href = '/sites/postDetails.php?id="+thema.id+"'\">"+thema.title+"</h2>\n" +
        "            <span>"+username+"</span>\n" +
        "        </div>\n" +
        "        <div class=\"list-element-description\">"+description+"</div>\n" +
        "    </div>\n" +
        "    <div class=\"list-element-buttons\">\n" +
        "        <i class=\"fa fa-lock\" style='visibility: "+visibility+"'></i>" +
        "    </div>\n" +
        "</div>");
}