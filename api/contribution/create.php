<?php
require_once __DIR__.'/../../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Connection;
use ForumApi\Database\Entity\Beitrag;
use ForumApi\Response;


$dotenv = new Dotenv(__DIR__.'/../..');
$dotenv->load();

$auth = new Auth(true);
if(!$auth->canEnterProtectedZone()) {
    $res = new Response(401, 'Unauthorized');
    $res->send();
    exit;
}
if(!isset($_POST['themaId']) || !isset($_POST['text']) || $_POST['text'] === '') {
    $res = new Response(400, 'Bad Request');
    $res->send();
    exit;
}
$auth->refreshExpiringDate();


try {
    $id = (isset($_POST['id'])) ? intval($_POST['id']) : null;
    $contr = new Beitrag(intval($_POST['themaId']), $_POST['text'], $auth->getUserId(), $id);
    $res = $contr->insert();
    Connection::getInstance()->disconnect();
} catch (Exception $e) {
    $res = new Response(500, $e->getMessage());
}
$res->send();