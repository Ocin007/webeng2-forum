<?php
require_once __DIR__.'/../../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Connection;
use ForumApi\Database\Entity\Beitrag;
use ForumApi\Database\Entity\Thema;
use ForumApi\Database\Entity\User;
use ForumApi\Response;


$dotenv = new Dotenv(__DIR__.'/../..');
$dotenv->load();

$auth = new Auth(true);
if(!$auth->canEnterProtectedZone()) {
    $res = new Response(401, 'Unauthorized');
    $res->send();
    exit;
}

if(!isset($_POST['posts']) || !isset($_POST['contributions']) ||
    ($_POST['posts'] !== 'true' && $_POST['posts'] !== 'false') ||
    ($_POST['contributions'] !== 'true' && $_POST['contributions'] !== 'false')) {
    $res = new Response(400, 'Bad Request');
    $res->send();
    exit;
}

try {
    $res = new Response();
    if($_POST['posts'] === 'true') {
        $thema = new Thema();
        $res->append(
            $thema->deleteWhereUserId($auth->getUserId())->getData()
        );
    }
    if($_POST['contributions'] === 'true') {
        $contr = new Beitrag();
        $res->append(
            $contr->deleteWhereUserId($auth->getUserId())->getData()
        );
    }
    $user = new User();
    $res->append(
        $user->deleteWhereUserId($auth->getUserId())->getData()
    );
    $auth->logout();
    Connection::getInstance()->disconnect();
} catch (Exception $e) {
    $res = new Response(500, $e->getMessage());
}
$res->send();