<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Connection;
use ForumApi\Response;


$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();

$auth = new Auth(false);
if(!$auth->canEnterProtectedZone()) {
    $res = new Response(401, 'Unauthorized');
    $res->send();
    exit;
}
if(isset($_POST['username']) && $_POST['username'] !== '' &&
    isset($_POST['password']) && $_POST['password'] !== '') {
    try {
        $res = $auth->login($_POST['username'], $_POST['password']);
    } catch (Exception $e) {
        $res = new Response(500, $e->getMessage());
    }
    $res->send();
} else {
    $res = new Response(400, 'Bad Request');
    $res->send();
}
try {
    Connection::getInstance()->disconnect();
} catch (Exception $e) {

}