<?php
require_once __DIR__.'/../../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Connection;
use ForumApi\Database\Entity\Thema;
use ForumApi\Response;


$dotenv = new Dotenv(__DIR__.'/../..');
$dotenv->load();

$auth = new Auth(true);
if(!$auth->canEnterProtectedZone()) {
    $res = new Response(401, 'Unauthorized');
    $res->send();
    exit;
}
if(!isset($_POST['id'])) {
    $res = new Response(400, 'Bad Request');
    $res->send();
    exit;
}
$auth->refreshExpiringDate();

try {
    $thema = new Thema();
    $res = $thema->toggleClosed(intval($_POST['id']));
    Connection::getInstance()->disconnect();
} catch (Exception $e) {
    $res = new Response(500, $e->getMessage());
}
$res->send();