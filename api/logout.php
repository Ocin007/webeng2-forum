<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Response;


$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();

$auth = new Auth(true);
if(!$auth->canEnterProtectedZone()) {
    $res = new Response(401, 'Unauthorized');
    $res->send();
    exit;
}
$auth->logout();
$res = new Response();
$res->send();