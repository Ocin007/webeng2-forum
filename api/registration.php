<?php
require_once __DIR__.'/../vendor/autoload.php';

use Dotenv\Dotenv;
use ForumApi\Auth;
use ForumApi\Database\Connection;
use ForumApi\Database\Entity\User;
use ForumApi\Response;


$dotenv = new Dotenv(__DIR__.'/..');
$dotenv->load();

$auth = new Auth(false);
if(!$auth->canEnterProtectedZone()) {
    $res = new Response(401, 'Unauthorized');
    $res->send();
    exit;
}
try {
    $user = new User($_POST['username'], $_POST['password'], $_POST['password2']);
    $res = $user->insert();
    Connection::getInstance()->disconnect();
} catch (Exception $e) {
    $res = new Response(500, $e->getMessage());
}
$res->send();